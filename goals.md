# Planning

## Modules

- datetime

## Data input / Variables

- Shift Duration [HH]
- Shift Start time [HH:MM:SS]
- Lunch start [HH:MM:SS]
- Lunch end time [HH:MM:SS]
  - Lunch_total = ( Lunch end - Lunch start )
-Shift_total = Shift Duration + Lunch_total

- Clock_out =  shift start + Shift_total

## Program flow

1. Prompts user for shift duration
2. Prompts user for start time
3. Prompts user for start & end of Lunch
4. Calculates clock out time [HH:MM:SS]

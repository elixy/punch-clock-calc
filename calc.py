import datetime

sd = shift_duration = int(input("How long is your shift?:  "))
ss = shift_start = input("What time did you clock in? [HH:MM:SS]:  ")
ls = lunch_start = input("What time did your lunch start? [HH:MM:SS]:  ")
le = lunch_end = input("What time did your lunch end? [HH:MM:SS]:  ")

shift_duration = datetime.timedelta(hours=int(shift_duration))
shift_start = datetime.datetime(2020, 1, 1, int(ss[0:2]), int(ss[3:5]), int(ss[6:8]))
lunch_start = datetime.datetime(2020, 1, 1, int(ls[0:2]), int(ls[3:5]), int(ls[6:8]))
lunch_end = datetime.datetime(2020, 1, 1, int(le[0:2]), int(le[3:5]), int(le[6:8]))

lunch_duration = lunch_end - lunch_start
shift_total = shift_duration + lunch_duration
clock_out = shift_start + shift_total
out = str(clock_out)
print("You Clock out at: ", end='')
print(out[11:])
